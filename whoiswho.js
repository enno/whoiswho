var Selected = "";

function SwitchMessage(elm)
{

	var elmref;
	if (Selected != "")
	{
		elmref = document.getElementById('c_'+Selected);
		if (elmref) elmref.style.display = 'none';
	}
	if (Selected != elm.name)
	{
		Selected = elm.name;
		elmref = document.getElementById('c_'+Selected);
		if (elmref)
		{
			if (elmref.style.display=='none' || elmref.style.display=='') elmref.style.display='block';
			else elmref.style.display = 'none';
		}
	}
	else
		Selected="";

	return false;
}

function SwitchThisMessage()
{
	return SwitchMessage(this);
}

function MakeComments()
{
	//var table = document.getElementById("page");
	var links = document.getElementsByTagName("a");
	for (var i = 0; i < links.length; i++)
	{
		if (links[i].id == "comment")
			links[i].onclick = SwitchThisMessage;
	}
	return true;
}

