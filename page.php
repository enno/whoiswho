<?php

class page
{
	var $title;
	var $css;

	var $mode;

	function __construct($str = "")
	{
		$this->title = $str;
		$this->mode = "init";

		echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\n   \"http://www.w3.org/TR/html4/loose.dtd\">\n";
		echo "<html>\n";
	}

	function title($str)
	{
		$this->title = $str;
	}

	function css($file)
	{
		$this->css = $file;
	}

	function head()
	{
		if ($this->mode != "init")
			return;
		$this->mode = "head";

		echo "<head>\n";
		echo "  <META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=utf-8\">\n";
		echo "  <title>".$this->title."</title>\n";
		if ($this->css)
			echo "  <link rel=\"stylesheet\" type=\"text/css\" href=\"".$this->css."\">\n";
			
		echo "  <link rel=\"SHORTCUT ICON\" href=\"/favicon.ico\">\n";
		echo "<script language=\"JavaScript\" src=\"whoiswho.js\" type=\"text/javascript\"></script>\n";
		echo "</head>\n";

		echo "<body>\n";
		echo "<div class=\"header\">\n";
	}

	function end_head()
	{
		if ($this->mode == "init")
			$this->head();
		if ($this->mode != "head")
			return;
		$this->mode = "headend";
		echo "</div>\n";
	}

	function left()
	{
		if ($this->mode == "init" || $this->mode == "head")
			$this->end_head();
		if ($this->mode != "headend")
			return;
		$this->mode = "left";

		echo "<div class=\"leftside\">\n";
	}	

	function end_left()
	{
		if ($this->mode == "init" || $this->mode == "head")
			$this->end_head();
		if ($this->mode != "left")
			return;
		$this->mode = "leftend";
		echo "</div>\n";
	}

	function body()
	{
		if ($this->mode == "init" || $this->mode == "head" || $this->mode == "left")
			$this->end_left();
		if ($this->mode != "headend" && $this->mode != "leftend")
			return;
		$this->mode = "body";
		echo "<div class=\"page\">\n";
	}

	function end_body()
	{
		if ($this->mode != "body")
			return;
		$this->mode = "bodyend";
		echo "</div>\n";
	}

	function stop()
	{
		if ($this->mode == "body")
			$this->end_body();
		echo "<script language=\"JavaScript\" type=\"text/javascript\">MakeComments();</script>\n";
		echo "</body>\n";
		echo "</html>\n";
	}
}

// Hilfsfunktionen
function alink($dest, $text, $title=false)
{
	if ($title)
		return "<a href=\"$dest\" title=\"$title\">$text</a>";
	else
		return "<a href=\"$dest\">$text</a>";
}

function img($src, $title=false)
{
	if($title)
		return "<img src=\"$src\" title=\"$title\">"; 
	else
		return "<img src=\"$src\">";
}

?>
