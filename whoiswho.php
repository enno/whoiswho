<?php

function entryCounter()
{
	global $db;
	if (isset($db))
	{
		$db->query("SELECT count(*) FROM user");
		$data = $db->fetch();
		$count = $data[0];
	}

	echo "<div class=\"side\">\n";

	if (isset($db))
	{
		global $url;
		echo "<p>Zur Zeit sind $count Steckbriefe auf <a href=\"$url\">$url</a> eingetragen.\n";
	}
	else
	{
		echo "<p><b>Es gibt Probleme mit der Datenbank. Versuch es später nochmal!</b>\n";
	}

	echo "</div>\n";
}

function birthdays()
{
	$count = 10; // die nächsten 10 Geburtstage

        global $db;
        if (!isset($db))
		return;

	$db->query("SELECT nickname,geburtstag,to_days(date_add(geburtstag,INTERVAL year(now())-year(geburtstag) YEAR))-to_days(now()) as days FROM user HAVING days>=0 ORDER BY days LIMIT $count");

	echo "<div class=\"side\">\n";
	echo "<div class=\"sidetitle\">Die n&auml;chsten $count Geburtstage</div>\n";

	echo "<br><div class=\"steckbrief\">";
	echo "<table border=\"0\" width=\"50%\" cellpadding=\"5\" class=\"steckbrieftab\">";
	echo "<tr><th class=\"sidetitle\">Nickname</th><th class=\"sidetitle\">In Tagen</th></tr>\n";

	while ($user = $db->dict())
	{
		$nick = $user["nickname"];
		echo "<tr><td class=\"label\">".alink("?mode=show&amp;nick=".htmlentities($nick),convertText($nick))."</td>";

		if ($user["days"] == 0)
			echo "<td class=\"text\">heute</td></tr>\n";
		else if ($user["days"] == 1)
			echo "<td class=\"text\">morgen</td></tr>\n";
		else
			echo "<td class=\"text\">".$user["days"]."</td></tr>\n";
	}

	if ($db->rows() < $count)
	{
		$count -= $db->rows();
		$db->query("SELECT nickname,geburtstag,to_days(date_add(geburtstag,INTERVAL year(now())-year(geburtstag)+1 YEAR))-to_days(now()) as days FROM user HAVING days>=0 ORDER BY days LIMIT $count");

    	while ($user = $db->dict())
	    {
	    	$nick = $user["nickname"];
			echo "<tr><td class=\"label\">".alink("?mode=show&amp;nick=".htmlentities($nick),convertText($nick))."</td>";

			if ($user["days"] == 0)
				echo "<td class=\"text\">heute</td></tr>\n";
			else if ($user["days"] == 1)
				echo "<td class=\"text\">morgen</td></tr>\n";
			else
				echo "<td class=\"text\">".$user["days"]."</td></tr>\n";
		}
	}

	echo "</table></div>\n";

        echo "</div>\n";
}

function search()
{
	$search_enc = @$_REQUEST["search"];
	$search = stripslashes($search_enc);
	$type = @$_REQUEST["type"];
	$fields = @$_REQUEST["fields"];

	echo "<div class=\"sidetitle\">WhoIsWho: Steckbriefe durchsuchen</div>\n";
	echo "<div class=\"side\">\n";

	echo "Steckbriefe nach Nickname oder komplett durchsuchen.<p>";

	echo "<form action=\"?\" method=\"GET\">\n";
	echo "<input type=\"hidden\" name=\"mode\" value=\"search\">\n";
	echo "Suchbegriff(e):<br><input type=\"text\" name=\"search\" value=\"$search\"><br>\n";
	echo "Wie suchen?<br><select name=\"type\">";
	echo "<option value=\"all\"".($type=="all"?" selected":"").">Alle W&ouml;rter";
	echo "<option value=\"one\"".($type=="one"?" selected":"").">Ein Wort";
	echo "<option value=\"full\"".($type=="full"?" selected":"").">Ganzer Satz";
	echo "</select><br>\n";
        echo "Wo suchen?<br><select name=\"fields\">";
        echo "<option value=\"nickname\"".($fields=="nickname"?" selected":"").">Nickname";
        echo "<option value=\"full\"".($fields=="full"?" selected":"").">Volltext";
        echo "</select><br>\n";
	echo "<p><input type=\"submit\" value=\"Suchen\">\n";
	echo "</form>\n";

	if (!trim($search))
	{
		echo "</div>";
		return;
	}

	echo "<div class=\"sidetitle\">Steckbriefe mit den Suchbegriffen '".htmlentities($search)."'</div>\n";

	// durchsuchen
        global $db;

	if ($type != "full")
		$words = explode(" ", $search_enc);
	else
		$words = array($search_enc);

	if ($type == "all")
		$conj = "AND";
	else
		$conj = "OR";

	$where = "";
	foreach ($words as $word)
	{
		if (!$word)
			continue;

		if (!$where)
			$where = "WHERE ";
		else
			$where .= " $conj ";

		if ($fields == "nickname")
			$where .= "(nickname LIKE '%$word%' OR alternate LIKE '%$word%')";
		else
			$where .= "(nickname LIKE '%$word%' OR alternate LIKE '%$word%' OR realname LIKE '%$word%' OR wohnort LIKE '%$word%' OR homepage LIKE '%$word%' OR beruf LIKE '%$word%' OR hobbies LIKE '%$word%' OR partei LIKE '%$word%' OR buendnis LIKE '%$word%' OR ort LIKE '%$word%' OR parteipage LIKE '%$word%' OR sonstiges LIKE '%$word%')";
	}

        $db->query("SELECT user.*, count(userid) AS comments FROM user LEFT OUTER JOIN comments ON user.id=userid $where GROUP BY user.id ORDER BY nickname");

	// eintraege anzeigen
        echo "Passende Einträge: ".$db->rows()."<p>\n";

        while ($row = $db->dict())
                show_data($row);

	echo "</div>";
}

function displayText($filename)
{
        if (!$filename)
                return;
        $filename=str_replace("..", "", $filename);
        if (substr($filename,0,1) == "/")
                return;

        $text = "";
        $file = @fopen($filename, "r");
        if ($file)
        {
                while ($line = fgets($file))
                        $text .= $line;
                fclose($file);
        }
        else
        {
                $text = "=== 404 File not found ===\nDie Datei konnte nicht gefunden werden.\n";
        }

        echo nl2br(convertText($text));
}

function convertText($text)
{
	// eliminate HTML chars...
	$text = htmlentities($text, ENT_SUBSTITUTE);

	// link urls...
	$parts = explode(" ", $text);

	for ($i = 0; $i < count($parts); $i++)
		if (substr($parts[$i],0,7) == "http://")
			$parts[$i] = alink($parts[$i],$parts[$i]);

	//$text = implode(" ", $parts);

	// bold, italic...
        $search = array("'\\*\\*(.*)\\*\\*'U",
                        "'__(.*)__'U",
                        "'(?<!http:)\/\/(.*)\/\/'U",
                        "'---'",
                        "'- - -'",
                        "'@@(.*)@@'U",
                        "'##(.*)##'Us",
                        "'[$][$](.*)[$][$]'U",
                        "'===(.*)===(\\n|)(((=|)([^=]+?))*?)'U",
                        "'(\[\[)?([^ \(\)\[\]\/@<>\"]+@[^ \(\)\[\]\/@<>\"]+\.[a-zA-Z]+)(\]\])?'",
                        "'(?<!\[\[)(http|ftp):\/\/([^ <>\(\)\[\],]*)'",
                        "'\[\[link:([^ <>\]]+)[ ]*\]\]'",
                        "'\[\[link:([^ <>\]]+)[ ]+(.+?)\]\]'",
			"'\[\[banner:world-of-dungeons:([0-9]+)\]\]'",
                        "'\[\[banner:eressea\]\]'",
                        "'\[\[banner:oots\]\]'",
                        "'\[\[(http://|)([^ <>\]]+)[ ]*\]\]'",
                        "'\[\[(http://|)([^ <>\]]+)[ ]+(.+?)\]\]'");

        $repl = array("<b>\\1</b>",
                        "<u>\\1</u>",
                        "<i>\\1</i>",
                        "<br>",
                        "<hr>",
                        "<strike>\\1</strike>",
                        "<tt>\\1</tt>",
                        "<img src=\"/cgi-bin/mimetex.cgi?\\1\" alt=\"Formel\">",
                        "<div class=\"sidetitle\">\\1</div><div class=\"side\">\\3</div>",
                        "<a href=\"mailto:\\2\" title=\"Mail an \\2\">\\2</a>",
                        "<a href=\"\\0\" title=\"\\0\">\\0</a>",
                        "<a href=\"\\1\" title=\"\\1\">\\1</a>",
                        "<a href=\"\\1\" title=\"\\2\">\\2</a>",
						"<a HREF=\"http://world-of-dungeons.de/?link_wa_\\1\" TARGET=\"_blank\"><img SRC=\"wodbanner.jpg\" BORDER=\"0\" ALT=\"World of Dungeons - das kostenlose Online-Spiel\" TITLE=\"World of Dungeons - das kostenlose Online-Spiel\" WIDTH=\"100\" HEIGHT=\"67\"></a>",
                        "<a HREF=\"http://eressea.de/\" TARGET=\"_blank\"><img SRC=\"ebanner.gif\" BORDER=\"0\" ALT=\"Eressea PBeM\" TITLE=\"Eressea PBeM\" WIDTH=\"152\" HEIGHT=\"52\"></a>",
                        "<a HREF=\"http://www.giantitp.com/cgi-bin/GiantITP/ootscript\" TARGET=\"_blank\"><img SRC=\"ootsbanner.gif\" BORDER=\"0\" ALT=\"Order Of The Stick\" TITLE=\"Order Of The Stick\" WIDTH=\"88\" HEIGHT=\"31\"></a>",
                        "<a href=\"http://\\2\" title=\"http://\\2\">\\2</a>",
                        "<a href=\"http://\\2\" title=\"http://\\2\">\\3</a>");

	$text = preg_replace($search, $repl, $text);

	return $text;
}

function show_data($data, $formdata = false)
{
	global $url, $label;

	$id = $data["id"];
	$nickraw = $data["nickname"];
	$nick = convertText($nickraw);
	$bild = $data["bild"];
	if (!$bild)
		$bild = "Kein Bild";
	else if (substr($bild,0,7) == "http://")
	{
		$bild = explode(" ", $bild);

                $sizedata = "";
                $bildhref = $bild[0];

		if ($bild[1] == "size")
		{
			$width = doubleval($bild[2]);
			$height = doubleval($bild[3]);

			global $imagemax;

			if ($width > $imagemax)
			{
				$height /= $width / $imagemax;
				$width /= $width / $imagemax;
			}
			if ($height > $imagemax)
			{
				$width /= $height / $imagemax;
				$height /= $height / $imagemax;
			}

			$width = intval($width);
			$height = intval($height);

			$sizedata = " width=\"$width\" height=\"$height\"";
		}
		else if (substr($bild[1],0,7) == "http://")
			$bildhref = $bild[1];

		$bild = str_replace($url, "", $bild[0]);
		$bildhref = str_replace($url, "", $bildhref);

		$bild = str_replace("http://whoiswho.42t.de/", "", $bild);
		$bildhref = str_replace("http://whoiswho.42t.de/", "", $bildhref);

		$bild = htmlentities($bild);
		$bildhref = htmlentities($bildhref);
		$bild = "<a href=\"$bildhref\"><img src=\"$bild\" border=\"0\" alt=\"$nick\" title=\"$nick\"$sizedata></a>";
	}
	else
		$bild = convertText($bild);

	$update = $data["lastupdate"];
	if ($update)
	{
        	$update = explode(" ", $update);
                $update[0] = array_reverse(explode("-", $update[0]));
                $update = implode(".", $update[0]);

		$update = " <font size=\"-2\">(Update: $update)</font>";
	}
	else
		$update = "";

	$controls = "";	// admin controls
	if (false)
	{
		$controls = " <a href=\"#\" class=\"speclink\" title=\"Löschen\">X</a>";
		$controls .= "  <a href=\"#\" class=\"speclink\" title=\"Passwort ändern\">&prod;</a>";
	}

	echo "<div class=\"steckbrief\">\n";
	echo "<table border=\"0\" width=\"100%\" cellpadding=\"5\" class=\"steckbrieftab\">\n";
	echo "<tr><td colspan=\"2\" class=\"sidetitle\" align=\"center\"><a name=\"".htmlentities($nickraw)."\">$nick$update <a href=\"?mode=show&amp;nick=".htmlentities($nickraw)."\" title=\"Zeige nur $nick\" class=\"speclink\">&perp;</a>$controls</a></td></tr>\n";
	echo "<tr><td width=\"30%\" align=\"center\" class=\"photo\">$bild</td>\n";

	echo "<td><table border=\"0\" cellpadding=\"5\">\n";

	foreach($data as $key => $value)
	{
		$value = utf8_encode($value);
		if ($value && $key != "id" && $key != "password" && $key != "email" && $key != "bild" && $key != "geburtstag" && $key != "lastupdate" && $key != "comments")
		{
			$labl = $label[$key];
			$value = convertText($value);
			echo "<tr><td class=\"label\">$labl</td><td class=\"text\">$value</td></tr>\n";
		}
		else if ($value && ($key == "geburtstag"))
		{
			$labl = $label[$key];
			$value = array_reverse(explode("-", $value));
			$value = implode(".", $value);
			if ($value == "00.00.0000")
				$value = "";
			else if (substr($value,0,6) == "00.00.")
				$value = substr($value,6,4);

			if ($value)
				echo "<tr><td class=\"label\">$labl</td><td class=\"text\">$value</td></tr>\n";
		}
	}

	echo "</table></td></tr>\n";
	echo "</table>\n";

	if (isset($data["comments"]))
		$count = $data["comments"];
	else
		$count = -1;	// unbekannte Anzahl

	global $db;
	if (isset($db) && $count)
	{
		$query = $db->query("SELECT * FROM comments WHERE userid=$id ORDER BY id", false);
		$count = $db->rows($query);
	}

	echo "<div class=\"commentswitch\"><a href=\"?mode=comment&amp;nick=".htmlentities($nickraw)."\" id=\"comment\" name=\"u$id\">".($count?"<b>$count</b>":"Keine")." Kommentare</a></div>\n";

	$commentstyle = "";
	if ($formdata)
		$commentstyle = " style=\"display:block;\"";

	echo "<div class=\"comments\"$commentstyle id=\"c_u$id\">\n";

	$controls = "";	// admin controls

	// Kommentare auflisten, wenn vorhanden
	if ($count)
	while ($comment = $db->dict($query))
	{
		$date = $comment["date"];
		if ($date)
		{
			$date = explode(" ", $date);
			$date[0] = array_reverse(explode("-", $date[0]));
			$date[0] = implode(".", $date[0]);
			$date[1] = substr($date[1], 0, strrpos($date[1], ":"));
			$date = implode(" ", $date);
		}
		else $date = "";

		$nick = convertText($comment["name"]);
		$link = htmlentities($comment["link"]);
		if ($link)
			$nick = alink($link, $nick, $link);

		if (false)	// admin controls
		{
			$controls = " - <a href=\"#\" class=\"speclink\" title=\"Löschen\">X</a>";
		}

		echo "<div class=\"commenttitle\">$date von $nick$controls</div>\n";
		echo "<div class=\"commentbody\">";
		echo nl2br(convertText($comment["text"]));
		echo "</div>\n";
	}

	// Eingabemaske
	$namevalue = $linkvalue = $textvalue = $error = "";
	if ($formdata)
	{
		if ($formdata["name"])
			$namevalue = " value=\"".$formdata["name"]."\"";
		if ($formdata["link"])
			$linkvalue = " value=\"".$formdata["link"]."\"";
		if ($formdata["text"])
			$textvalue = $formdata["text"];
		if ($formdata["error"])
			$error = $formdata["error"];
	}

	echo "<div class=\"commenttitle\">Kommentar schreiben</div>\n";
	echo "<div class=\"commentbody\">";
	echo "<form action=\"?\" method=\"post\">";
	echo "<input type=\"hidden\" name=\"mode\" value=\"comment\">";
	echo "<input type=\"hidden\" name=\"nick\" value=\"".htmlentities($nickraw)."\">";
	echo "<input type=\"hidden\" name=\"uid\" value=\"$id\">";
	echo "Nickname: <input type=\"text\" size=\"20\" name=\"name\"$namevalue>, ";
	echo "Passwort: <input type=\"password\" size=\"20\" name=\"pass\">\n";
	echo "<p/>eMail/Homepage: <input type=\"text\" size=\"40\" name=\"link\"$linkvalue>\n";
	echo "<br/>Text:";
	echo "<br/><textarea class=\"commentarea\" name=\"text\">$textvalue</textarea>\n";
	echo "<br/>$error\n";
	echo "<br/><input type=\"submit\" name=\"submit\" value=\"Kommentieren\"></form></div>\n";

	echo "</div>\n";
	echo "</div>\n";
}

function list_nicknames()
{
        echo "<div class=\"sidetitle\">WhoIsWho: Liste der Nicknames</div>\n";
        echo "<div class=\"side\">\n";

	echo "Liste der eingetragenen Nicknames:<p>\n";

        global $db;
        $db->query("SELECT nickname FROM user ORDER BY nickname");

        echo "Daten&auml;tze: ".$db->rows()."<p>\n";

	echo "<ul>\n";
	while ($data = $db->dict())
	{
		$nick = $data["nickname"];
		echo "<li>".alink("?mode=show&amp;nick=".htmlentities($nick),convertText($nick))."</li>\n";
	}
	echo "</ul>\n";

	echo "</div>";
}

function show_entry()
{
	$nick = $_REQUEST["nick"];

	if (!$nick)
	{
		list_all();
		return;
	}

        global $db;
        $db->query("SELECT user.*, count(userid) AS comments FROM user LEFT OUTER JOIN comments ON user.id=userid WHERE nickname LIKE '$nick' GROUP BY user.id");

	if (!$db->rows())
	{
   		echo "<div class=\"sidetitle\">WhoIsWho: Steckbrief $nick nicht gefunden.</div>\n";
        	echo "<div class=\"side\">\n";

		echo "Der Steckbrief $nick befindet sich nicht in der Datenbank.\n";

	        echo "</div>";
		return;
	}

        echo "<div class=\"sidetitle\">WhoIsWho: Steckbrief von ".convertText($nick)."</div>\n";
        echo "<div class=\"side\">\n";

	show_data($db->dict());

	echo "</div>";
}

function show_recent()
{
        echo "<div class=\"sidetitle\">WhoIsWho: Zuletzt ge&auml;nderte Steckbriefe</div>\n";
        echo "<div class=\"side\">\n";

	echo "Hier stehen die Steckbriefe, die innerhalb eines Tages, einer Woche oder 30 Tagen ge&auml;ndert wurden.<p>\n";

        global $db;

        $db->query("SELECT user.*, count(userid) AS comments FROM user LEFT OUTER JOIN comments ON user.id=userid WHERE lastupdate >= DATE_SUB(now(),INTERVAL 1 DAY) GROUP BY user.id ORDER BY lastupdate DESC");

	if ($db->rows())
	{
		echo "<div class=\"sidetitle\">Innerhalb eines Tages ge\xe4ndert</div>\n";
	        echo "Datens&auml;tze: ".$db->rows()."<br>\n";

		while ($row = $db->dict())
        		show_data($row);
	}

        $db->query("SELECT user.*, count(userid) AS comments FROM user LEFT OUTER JOIN comments ON user.id=userid WHERE lastupdate < DATE_SUB(now(),INTERVAL 1 DAY) AND lastupdate >= DATE_SUB(now(),INTERVAL 7 DAY) GROUP BY user.id ORDER BY lastupdate DESC");

	if ($db->rows())
	{
		echo "<div class=\"sidetitle\">Innerhalb einer Woche ge\xe4ndert</div>\n";
	        echo "Datens&auml;tze: ".$db->rows()."<br>\n";

        	while ($row = $db->dict())
                	show_data($row);
	}

	$db->query("SELECT user.*, count(userid) AS comments FROM user LEFT OUTER JOIN comments ON user.id=userid WHERE lastupdate < DATE_SUB(now(),INTERVAL 7 DAY) AND lastupdate >= DATE_SUB(now(),INTERVAL 30 DAY) GROUP BY user.id ORDER BY lastupdate DESC");

	if ($db->rows())
	{
		echo "<div class=\"sidetitle\">Innerhalb von 30 Tagen ge&auml;ndert</div>\n";
		echo "Datens&auml;tze: ".$db->rows()."<br>\n";

		while ($row = $db->dict())
			show_data($row);
	}

        echo "</div>";
}

function show_comments()	// show recent comments
{
        echo "<div class=\"sidetitle\">WhoIsWho: Liste der Kommentare</div>\n";
        echo "<div class=\"side\">\n";

	echo "Hier stehen alle Kommentare, sortiert nach Datum des Eintrags.<p>\n";

        global $db;

		$db->query("SELECT comments.*, user.nickname, user.id FROM comments, user WHERE user.id=comments.userid ORDER BY date DESC");

		if ($db->rows())
		{
			echo "<div class=\"sidetitle\">Die Liste aller Kommentare</div>\n";
	        echo "Datens&auml;tze: ".$db->rows()."<br>\n";
		}

		while ($comment = $db->dict())
		{
			$date = $comment["date"];
			if ($date)
			{
				$date = explode(" ", $date);
				$date[0] = array_reverse(explode("-", $date[0]));
				$date[0] = implode(".", $date[0]);
				$date[1] = substr($date[1], 0, strrpos($date[1], ":"));
				$date = implode(" ", $date);
			}
			else $date = "";

			$userraw = $comment["nickname"];
			$user = convertText($userraw);

			$nick = convertText($comment["name"]);
			$link = htmlentities($comment["link"]);
			if ($link)
				$nick = alink($link, $nick, $link);

			echo "<div class=\"commentlist\">\n";

			echo "<div class=\"commenttitle\">Über <a href=\"?mode=comment&amp;nick=".htmlentities($userraw)."\">$user</a>, $date von $nick</div>\n";
			echo "<div class=\"commentsbody\">";
			echo nl2br(convertText($comment["text"]));
			echo "</div>\n";
			echo "</div>\n";
		}
}

function list_all()
{
	echo "<div class=\"sidetitle\">WhoIsWho: Alle Steckbriefe</div>\n";
	echo "<div class=\"side\">\n";

	global $db;

	$db->query("SELECT user.*, count(userid) AS comments FROM user LEFT OUTER JOIN comments ON user.id=userid GROUP BY user.id ORDER BY nickname");

	echo "Datens&auml;tze: ".$db->rows()."<br>\n";

	echo "<center>";
	$letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$dict["|"] = "Sonderzeichen";
        $dict["Ä"] = "Sonderzeichen";
        $dict["Ö"] = "Sonderzeichen";
        $dict["Ü"] = "Sonderzeichen";
	for ($i = 0; $i < 26; $i+=2)
	{
		$letter = $letters[$i]."-".$letters[$i+1];
		$dict[$letters[$i]] = $letter;
		$dict[$letters[$i+1]] = $letter;
		echo "<a href=\"#letter$letter\" title=\"$letter\">$letter</a>&nbsp;";
	}
	echo "</center><br>\n";

	$letter = "";
	while ($row = $db->dict())
	{
		if ($dict[strtoupper($row["nickname"][0])] != $letter)
		{
			$letter = $dict[strtoupper($row["nickname"][0])];
			echo "<div class=\"sidetitle\"><a name=\"letter".htmlentities($letter)."\">Steckbriefe mit ".htmlentities($letter)."</a>&nbsp;&nbsp;&nbsp;<a href=\"#\" class=\"speclink\">&uArr;</a></div>\n";
		}
		show_data($row);
	}

	echo "</div>";
}

function new_entry()
{
	echo "<div class=\"sidetitle\">WhoIsWho: Eintrag erstellen</div>\n";
	echo "<div class=\"side\">\n";

	echo "Um einen Steckbrief zu erstellen, muss man hier den Nicknamen ";
	echo "und eine gültige eMail-Adresse eingeben. An die eMail-Adresse ";
	echo "wird das Passwort geschickt, mit dem man sich dann einloggen und ";
	echo "die Daten eingeben und verändern kann.";

	echo "<p>";

	echo "<form action=\"?\" method=\"POST\">\n";
	echo "Nickname:<br><input type=\"text\" name=\"nickname\" value=\"\"><br>\n";
	echo "eMail:<br><input type=\"text\" name=\"email\" value=\"\"><br>\n";
	echo "<p><input type=\"submit\" value=\"Erstellen\">\n";
	echo "<input type=\"hidden\" name=\"mode\" value=\"create\">\n";
	echo "</form>\n";

	echo "</div>";
}

function make_and_send_passwd($nick, $email, $pass = false)
{
	global $db;

	if (!$pass)
	{
        	$pass = createpasswd();

 	       $res = $db->query("UPDATE user SET password=SHA('$pass') WHERE nickname='$nick'");

        	if (!$res)
       	 	{
                	echo "<div class=\"sidetitle\">WhoIsWho: Konnte Passwort nicht ändern</div>\n";
                	echo "<div class=\"side\">\n";
                	echo "Fehler beim Updaten der Datenbank.";
                	echo "</div>";
                	return false;
		}
        }

	global $url;

        $res = mail($email,"[WhoIsWho] Steckbrief: $nick","Das Passwort für den Nickname $nick ist:\n\n$pass\n\nLogin: $url?mode=login\n\nDas WhoIsWho des IRC-Channels #Eressea\n$url","From: Eressea-WhoIsWhoo <whoiswho@draig.de>");
        if (!$res)
        {
                echo "<div class=\"sidetitle\">WhoIsWho: Fehler beim Versenden der Mail</div>\n";
                echo "<div class=\"side\">\n";
                echo "Die Best\xe4tigungsmail konnte nicht versendet werden.";
                echo "</div>";
                return false;
        }

	return true;
}

function create_entry()
{
	$nick = $_REQUEST["nickname"];
	$email = $_REQUEST["email"];

	if (strlen($nick) < 3 || strlen($email) < 7 || !strchr($email,"@"))
	{
		echo "<div class=\"sidetitle\">WhoIsWho: Ungültige Eingabe</div>\n";
		echo "<div class=\"side\">\n";
		echo "Der Nickname '$nick' oder die eMail-Adresse '$email' ist ungültig.";
		echo "</div>";
		return;
	}

	global $db;
	$db->query("SELECT nickname FROM user WHERE nickname LIKE '$nick'");

	if ($db->rows())
	{
		echo "<div class=\"sidetitle\">WhoIsWho: Nickname bereits vorhanden</div>\n";
		echo "<div class=\"side\">\n";
		echo "Der Nickname '$nick' ist bereits in der Datenbank.";
		echo "</div>";
		return;
	}

	$pass = createpasswd();

	$res = $db->query("INSERT INTO user SET nickname='$nick',email='$email',lastupdate=now(),password=SHA('$pass')");

	if (!$res)
	{
		echo "<div class=\"sidetitle\">WhoIsWho: Eintrag konnte nicht erstellt werden</div>\n";
		echo "<div class=\"side\">\n";
		echo "Fehler beim Updaten der Datenbank.";
		echo "</div>";
		return;
	}

	if (!make_and_send_passwd($nick,$email,$pass))
		return;

	echo "<div class=\"sidetitle\">WhoIsWho: Neuer Steckbrief erstellt</div>\n";
	echo "<div class=\"side\">\n";

	echo "Der Steckbrief für den Nickname ".convertText($nick)." wurde erstellt und das ";
	echo "Passwort an die eMail-Adresse $email geschickt.";

	echo "</div>";
}

function login()
{
        echo "<div class=\"sidetitle\">WhoIsWho: Login</div>\n";
        echo "<div class=\"side\">\n";

	echo "Nickname und Passwort eingeben, um den Steckbrief zu bearbeiten.";
        echo "<p>";

        echo "<form action=\"?\" method=\"POST\">\n";
        echo "Nickname:<br><input type=\"text\" name=\"login\" value=\"\"><br>\n";
        echo "Passwort:<br><input type=\"password\" name=\"pass\" value=\"\"><br>\n";
        echo "<p><input type=\"submit\" value=\"Login\">\n";
        echo "<input type=\"hidden\" name=\"mode\" value=\"edit\">\n";
        echo "</form>\n";

	echo "&nbsp;<p>&nbsp;<p>&nbsp;<p>&nbsp;<p>";
	echo "Passwort vergessen? Nickname und (gespeicherte!) eMail-Adresse eingeben, um ein neues anzufordern.";

	echo "<p><b>Von Xanathos importierte Steckbriefe</b> haben natürlich weder Passwort noch eMail-Adresse. Wer so einen Eintrag bearbeiten möchte, schickt mir am Besten eine Mail an <a href=\"mailto:whoiswho@draig.de\">WhoIsWho@draig.de</a>, damit ich ihm/ihr ein Passwort erstellen und zuschicken kann.<br>\n";

        echo "<form action=\"?\" method=\"POST\">\n";
        echo "Nickname:<br><input type=\"text\" name=\"login\" value=\"\"><br>\n";
        echo "eMail-Adresse:<br><input type=\"text\" name=\"email\" value=\"\"><br>\n";
        echo "<p><input type=\"submit\" value=\"Passwort schicken\">\n";
        echo "<input type=\"hidden\" name=\"mode\" value=\"edit\">\n";
	echo "<input type=\"hidden\" name=\"newpass\" value=\"true\">\n";
        echo "</form>\n";

        echo "</div>";
}

function edit_entry()
{
	global $db, $label;

	$login = $_REQUEST["login"];
	$pass = $_REQUEST["pass"];
	$save = $_REQUEST["save"];
	$newpass = $_REQUEST["newpass"];

	if ($newpass == "true")
	{
		$email = $_REQUEST["email"];

        	$res = $db->query("SELECT id,nickname FROM user WHERE nickname='$login' AND email='$email'");
        	if (!$db->rows())
		{
	                echo "<div class=\"sidetitle\">WhoIsWho: Nickname und eMail-Adresse passen nicht</div>\n";
                	echo "<div class=\"side\">\n";
                	echo "Die Kombination Nickname / eMail-Adresse gibt es nicht in der Datenbank.";
                	echo "</div>";
                	return;
		}

		if (!make_and_send_passwd($login,$email))
			return;

                echo "<div class=\"sidetitle\">WhoIsWho: Neues Passwort geschickt</div>\n";
                echo "<div class=\"side\">\n";
                echo "Das Passwort für den Steckbrief $login wurde geändert und an die eMail-Adresse $email geschickt.";
                echo "</div>";
                return;
	}

	// check password
	$res = $db->query("SELECT id,nickname FROM user WHERE nickname='$login' AND password=SHA('$pass')");
	if (!$db->rows())
	{
	        echo "<div class=\"sidetitle\">WhoIsWho: Login oder Passwort falsch</div>\n";
 	       	echo "<div class=\"side\">\n";
		echo "Nickname oder Passwort ist falsch.";
		echo "</div>";
		return;
	}

	$uid = $db->dict();
	$uid = $uid["id"];

	if ($save == "1")
	{
		// daten speichern
		$fields = array("nickname", "alternate", "email", "realname", "wohnort", "geburtstag", "homepage", "beruf", "hobbies", "partei", "buendnis", "ort", "parteipage", "sonstiges");

		$newnick = $_REQUEST["nickname"];
		if ($newnick && $newnick == $login)
			$newnick = "";
		else if ($newnick)
		{
			$res = $db->query("SELECT id,nickname FROM user WHERE nickname LIKE '$newnick' AND nickname NOT LIKE '$login'");
			if ($db->rows())
			{
				$newnick = "";
				echo "<font color=\"red\">Der Nickname konnte nicht geändert werden, da es schon einen Steckbrief mit diesem Nickname gibt.</font><p>\n";
			}
		}

		$update = "";
		foreach($fields as $field)
		{
			$value = $_REQUEST[$field];
			if ($field == "geburtstag")
			{
				if (!strchr($value,".") && strlen($value)==4)
					$value = "00.00.$value";
                		$value = array_reverse(explode(".", $value));
                		$value = implode("-", $value);
			}
			else if ($field == "nickname")
			{
				// nickname muss gueltig sein.
				$value = $newnick;
				if (!$value)
					continue;
			}
			$update .= ",$field='$value'";
		}

		$res = $db->query("UPDATE user SET lastupdate=now()$update WHERE id=$uid");
		if (!$res)
			echo "<font color=\"red\">Fehler beim Speichern der Daten.</font><p>\n";
		else
			echo "<font color=\"green\"><b>Daten gespeichert.</b></font><p>\n";
	}

	$db->query("SELECT * FROM user WHERE id=$uid");
	$data = $db->dict();

	$login = $data["nickname"];
	$nick = convertText($login);
	$update = $data["lastupdate"];

        echo "<div class=\"sidetitle\">WhoIsWho: Bearbeite Steckbrief $nick</div>\n";
        echo "<div class=\"side\">\n";

        if ($update)
        {
                $update = explode(" ", $update);
                $update[0] = array_reverse(explode("-", $update[0]));
                $update = implode(".", $update[0]);

                echo "<font size=\"-2\">Letzte Änderung: $update</font><p>";
        }

        echo "<form action=\"?\" method=\"POST\">\n";
	echo "<input type=\"hidden\" name=\"mode\" value=\"edit\">\n";
	echo "<input type=\"hidden\" name=\"save\" value=\"1\">\n";
	echo "<input type=\"hidden\" name=\"login\" value=\"$login\">\n";
	echo "<input type=\"hidden\" name=\"pass\" value=\"$pass\">\n";

	echo "<table border=\"0\">\n";

	echo "<tr><td>".$label["nickname"]."</td>\n";
	echo "<td><input type=\"text\" size=\"50\" name=\"nickname\" value=\"";
	echo htmlentities($data["nickname"])."\"/></td></tr>\n";

        echo "<tr><td>".$label["email"]."</td>\n";
        echo "<td><input type=\"text\" size=\"50\" name=\"email\" value=\"";
        echo htmlentities($data["email"])."\"/></td></tr>\n";

	echo "<tr><td>&nbsp;</td></tr>\n";

	foreach ($data as $key => $value)
	{
		if ($key != "id" && $key != "password" && $key != "lastupdate" && $key != "geburtstag" && $key != "nickname" && $key != "email" && $key != "bild")
		{
			$labl = $label[$key];
			if (!$labl) $labl = $key;
			echo "<tr><td>$labl</td>\n";
			echo "<td><input type=\"text\" size=\"50\" name=\"$key\" value=\"";
			echo htmlentities($value)."\"/></td></tr>\n";
		}
		else if ($key == "geburtstag")
		{
			$value = implode(".", array_reverse(explode("-", $value)));
                        $labl = $label[$key];
                        echo "<tr><td>$labl</td>\n";
                        echo "<td><input type=\"text\" size=\"50\" name=\"$key\" value=\"";
                        echo htmlentities($value)."\"/></td></tr>\n";
		}
	}
	echo "</table>\n";

        echo "<p><input type=\"submit\" value=\"Speichern\">\n";
        echo "</form>\n";

	// Bild hochladen

	echo "<p><br><div class=\"sidetitle\">Bild hochladen</div>\n";

	global $uploadmax;
	$upmax = "".intval($uploadmax/1024)." kb";

	echo "<p>Hier kann man ein Foto als GIF, PNG oder JPG hochladen.\n";
	echo "<br>Zur Zeit sind nur Bilder bis zu einer <b>Dateigröße von $upmax</b> erlaubt.";

        echo "<form enctype=\"multipart/form-data\" action=\"?\" method=\"POST\">\n";
        echo "<input type=\"hidden\" name=\"mode\" value=\"upload\"/>\n";
        echo "<input type=\"hidden\" name=\"login\" value=\"$login\"/>\n";
        echo "<input type=\"hidden\" name=\"pass\" value=\"$pass\"/>\n";
	echo "<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"$uploadmax\"/>\n";
	echo "<input type=\"file\" name=\"upload\"/ size=\"50\">\n";
        echo "<input type=\"submit\" value=\"Bild hochladen\"/>\n";
        echo "</form>\n";

        echo "<p>Wer sein Bild löschen möchte, klickt hier:\n";
        echo "<form action=\"?\" method=\"POST\">\n";
        echo "<input type=\"hidden\" name=\"mode\" value=\"upload\"/>\n";
        echo "<input type=\"hidden\" name=\"login\" value=\"$login\"/>\n";
        echo "<input type=\"hidden\" name=\"pass\" value=\"$pass\"/>\n";
        echo "<input type=\"submit\" value=\"Bild löschen\"/>\n";
	echo "&nbsp;<i>Vorsicht: Keine Sicherheitsabfrage!</i>\n";
        echo "</form>\n";

	// loeschen...

	echo "<p><br><div class=\"sidetitle\">Steckbrief löschen</div>\n";

	echo "<p>Hiermit kann ein Steckbrief endgültig <b>GELÖSCHT</b> werden:<br>\n";
	echo "<form action=\"?\" method=\"POST\">\n";
        echo "<input type=\"hidden\" name=\"mode\" value=\"delete\">\n";
        echo "<input type=\"hidden\" name=\"login\" value=\"$login\">\n";
        echo "<input type=\"hidden\" name=\"pass\" value=\"$pass\">\n";
	echo "<input type=\"submit\" value=\"Steckbrief löschen\">\n";
 	echo "</form>\n";

	echo "</div>";
}

function comment_entry()
{
	global $db;
	$nick = $_REQUEST["nick"];

	if (!$nick)
	{
		return;
	}

	if (isset($_REQUEST["uid"]) && $_REQUEST["uid"])
		$id = $_REQUEST["uid"];

	if (isset($id))
	{
		// Kommentar eintragen
		$name = $_REQUEST["name"];
		$pass = $_REQUEST["pass"];
		$link = $_REQUEST["link"];
		$text = $_REQUEST["text"];
		$error = "";
		$passOK = false;

		// check password
        	$res = $db->query("SELECT id,nickname FROM user WHERE nickname='$name' AND password=SHA('$pass')");
        	if ($db->rows())
			$passOK = true;

		if (!$name)
			$error = "Es muss ein Name angegeben werden!";
		else if (!$passOK)
			$error = "Falsches Passwort!";
		else if (!$text)
			$error = "Es muss ein Text eingegeben werden!";
		else if ($link)
		{
			if (strstr($link, "@"))
				$link = "mailto:$link";
			else if (strstr($link, "http://"))
			{ } // do nothing
			else
				$link = "http://$link";
		}

		if (!$error)
		{
			$res = $db->query("INSERT INTO comments SET userid='$id',name='$name',link='$link',date=now(),text='$text'");
			if (!$res)
			{
				echo "<div class=\"sidetitle\">WhoIsWho: Kommentar konnte nicht erstellt werden</div>\n";
				echo "<div class=\"side\">\n";
				echo "Fehler beim Updaten der Datenbank.";
				echo "</div>";
				return;
			}

			$text = "";
		}
	}

	// Eintrag anzeigen
	$db->query("SELECT * FROM user WHERE nickname LIKE '$nick'");

	if (!$db->rows())
	{
		echo "<div class=\"sidetitle\">WhoIsWho: Steckbrief $nick nicht gefunden.</div>\n";
		echo "<div class=\"side\">\n";

		echo "Der Steckbrief $nick befindet sich nicht in der Datenbank.\n";

		echo "</div>";
		return;
	}

	echo "<div class=\"sidetitle\">WhoIsWho: Steckbrief von ".convertText($nick)."</div>\n";
	echo "<div class=\"side\">\n";

	$formdata = array("name" => "");
	if (isset($id))
	{
		$formdata["name"] = htmlentities($name);
		$formdata["link"] = htmlentities($link);
		$formdata["text"] = htmlentities($text);
		$formdata["error"] = "<font color=\"red\">".htmlentities($error)."</font>";
	}

	show_data($db->dict(), $formdata);

	echo "</div>";
}

function upload_file()
{
	$ftype = $_FILES['upload']['type'];
        $fsize = $_FILES['upload']['size'];
        $fname = $_FILES['upload']['name'];
        $ftmp = $_FILES['upload']['tmp_name'];

        $login = $_REQUEST["login"];
        $pass = $_REQUEST["pass"];

        // check password
	global $db;
        $res = $db->query("SELECT id,nickname FROM user WHERE nickname='$login'
AND password=SHA('$pass')");
        if (!$db->rows())
        {
                echo "<div class=\"sidetitle\">WhoIsWho: Login oder Passwort fal
sch</div>\n";
                echo "<div class=\"side\">\n";
                echo "Nickname oder Passwort ist falsch.";
                echo "</div>";
                return;
        }

        $uid = $db->dict();
        $uid = $uid["id"];

	global $uploadmax;
	if ($fsize > $uploadmax)
	{
		echo "<div class=\"sidetitle\">WhoIsWho: Datei zu groß.</div>\n";
		echo "<div class=\"side\">\n";
		echo "Die hochgeladene Datei ist zu groß. Dateiupload ist nur bis $uploadmax Bytes erlaubt.";
                echo "</div>";
                return;
	}

	$endung = "";
	if ($ftype == "image/png")
		$endung = "png";
	else if ($ftype == "image/gif")
		$endung = "gif";
	else if ($ftype == "image/jpeg")
		$endung = "jpg";
	else if ($ftmp)
	{
		$info = @getimagesize($ftmp);

		if ($info[2] == 1)
			$endung = "gif";
		else if ($info[2] == 2)
			$endung = "jpg";
		else if ($info[2] == 3)
			$endung = "png";

		if ($endung)
		{
			if ($endung == "jpg")
				$ftype = "image/jpeg";
			else
				$ftype = "image/$endung";
		}
	}

        if (!$endung && $ftype)
        {
                echo "<div class=\"sidetitle\">WhoIsWho: Dateityp nicht erkannt</div>\n";
                echo "<div class=\"side\">\n";
                echo "Die hochgeladene Datei gehört zu keinem zugelassenem Typ. Es dürfen nur Bilder im PNG, GIF und JPEG-Format hochgeladen werden.";

		echo "<p>Mime-Type: $ftype\n";
                echo "</div>";
                return;
        }

	global $path, $url;

	if ($ftype)
	{
		$newpath = $path."img/$login.$endung";

		move_uploaded_file($ftmp, $newpath) or die("<font color=red>Fehler beim Kopieren der Datei.</font>");

		$urlpath = "img/$login.$endung";
		$urlpath = ereg_replace("[^a-zA-Z0-9:\/.-]", "_", $urlpath);

		$urlpath_tn = "img/thumbs/$login.jpg";
		$urlpath_tn = ereg_replace("[^a-zA-Z0-9:\/.-]", "_", $urlpath_tn);

		global $imagemax;
		$sample = $imagemax."x".$imagemax;
                $cmd = "convert -quality 90 -sample $sample ".$path.$urlpath." ".$path.$urlpath_tn;
                exec($cmd);

		// für Datenfeld umwandeln
		$urlpath = $url.$urlpath_tn." ".$url.$urlpath;
	}
	else
	{
		if (@unlink($path."img/$login.jpg")) echo "Bild gelöscht\n";
		if (@unlink($path."img/$login.png")) echo "Bild gelöscht\n";
		if (@unlink($path."img/thumbs/$login.jpg")) echo "Thumbnail gelöscht\n";
		echo "<br>\n";

		$urlpath = "";
	}

	echo "<div class=\"sidetitle\">WhoIsWho: Bilderupload für ".htmlentities($login)."</div>\n";
        echo "<div class=\"side\">\n";

	$update = ",bild='$urlpath'";

	$res = $db->query("UPDATE user SET lastupdate=now()$update WHERE id=$uid");
        if (!$res)
                echo "<font color=\"red\">Fehler beim Speichern der Daten.</font><p>\n";
        else
                echo "<font color=\"green\"><b>Daten gespeichert.</b></font><p>\n";

	echo "<a href=\"?mode=show&nick=$login\">Den Steckbrief von ".htmlentities($login)." ansehen</a>.\n";

	echo "</div>\n";
}


function delete_entry()
{
        global $db;

        $login = $_REQUEST["login"];
        $pass = $_REQUEST["pass"];
	$ack = $_REQUEST["ack"];

        $db->query("SELECT nickname FROM user WHERE nickname='$login' AND password=SHA('$pass')");
        if (!$db->rows())
        {
                echo "<div class=\"sidetitle\">WhoIsWho: Login oder Passwort falsch</div>\n";
                echo "<div class=\"side\">\n";
                echo "Nickname oder Passwort ist falsch.";
                echo "</div>";
                return;
        }

	$nick = convertText($login);

	if ($ack == "true")
	{
		// jetzt wirklich löschen
		$res = $db->query("DELETE FROM user WHERE nickname='$login'");
		if (!$res)
		{
			echo "Fehler beim Löschen...";
			return;
		}

		echo "<div class=\"sidetitle\">WhoIsWho: Steckbrief $nick gelöscht</div>\n";
		echo "<div class=\"side\">\n";
                echo "Der Steckbrief $nick wurde gelöscht.";
                echo "</div>";
                return;
	}

        echo "<div class=\"sidetitle\">WhoIsWho: Steckbrief $nick löschen</div>\n";
        echo "<div class=\"side\">\n";

	echo "Um das Löschen zu bestätigen, bitte noch einmal das Passwort eingeben:<p>\n";

        echo "<form action=\"?\" method=\"POST\">\n";
        echo "<input type=\"hidden\" name=\"mode\" value=\"delete\">\n";
	echo "<input type=\"hidden\" name=\"ack\" value=\"true\">\n";
        echo "<input type=\"text\" name=\"login\" value=\"$login\">\n";
        echo "<input type=\"password\" name=\"pass\" value=\"\">\n";
        echo "<input type=\"submit\" value=\"Steckbrief l\xf6schen\">\n";
        echo "</form>\n";

        echo "</div>";
}

?>
