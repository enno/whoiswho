<?php

class database
{
	var $conn;
	var $server;

	var $count;	// counts querys

	var $name;
	var $last;	// last query result

	function __construct($user, $pass, $serv = "localhost")
	{
		$this->server = $serv;

		$this->conn = mysqli_connect($this->server, $user, $pass);
		//if ($this->conn == false)
		//	echo "Konnte keine Verbindung zum Datenbankserver aufbauen: ".mysql_error();

		$this->count = 0;
	}

	function db($database)
	{
		$res = mysqli_select_db($this->conn, $database);
		//if ($res == false)
		//	echo "Auswahl der Datenbank fehlgeschlagen: ".mysql_error();
		return ($res != false);
	}

	function query($str, $save = true)
	{
		$this->count++;
		$res = mysqli_query($this->conn, $str);
		if ($save)
			$this->last = $res;
		return $res;
	}

	function fetch($res = 0)
	{
		if ($res == 0)
			return mysqli_fetch_array($this->last);
		return mysqli_fetch_array($res);
	}

        function dict($res = 0)
        {
                if ($res == 0)
                        return mysqli_fetch_assoc($this->last);
                return mysqli_fetch_assoc($res);
        }

	function free($res = 0)
	{
		if ($res == 0)
			return mysqli_free_result($this->last);
		return mysqli_free_result($res);
	}

	function rows($res = 0)
	{
		if ($res == 0)
			return mysqli_num_rows($this->last);
		return mysqli_num_rows($res);
	}

	function count()
	{
		return $this->count;
	}
}

function createpasswd() {
    $newpass = "";
    $laenge=7;
    $string="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    mt_srand((double)microtime()*1000000);

    for ($i=1; $i <= $laenge; $i++) {
        $newpass .= substr($string, mt_rand(0,strlen($string)-1), 1);
    }

    return $newpass;
}

?>
