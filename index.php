<?php

include("page.php");
include("mysql.php");
include("config.php");
include("whoiswho.php");

global $db;
$db = new database($user, $pass);
if (!$db->db($dbname))
{
	unset($db);
}

$page = new page;
$page->title("Eressea-WhoIsWho");
$page->css("style.css");

$page->head();

echo "<div class=\"side\"><span class=\"title\">Das WhoIsWho des Channels #Eressea</span></div>";

$page->left();

displayText("menu.txt");

$page->body();

if (!isset($db))
{
	// couldn't connect to database
	// show only 'start' page
        displayText("start.txt");

        entryCounter();

	$page->stop();
	exit();
}

// switch
if (isset($_REQUEST["mode"]))
	$mode = $_REQUEST["mode"];
else
	$mode = "";

if ($mode == "search")
{
	search();
}
else if ($mode == "list")
{
	list_all();
}
else if ($mode == "show")
{
	show_entry();
}
else if ($mode == "nnlist")
{
	list_nicknames();
}
else if ($mode == "recent")
{
	show_recent();
}
else if ($mode == "comments")
{
	show_comments();	// recent comments
}
else if ($mode == "new")
{
	new_entry();
}
else if ($mode == "create")
{
	create_entry();
}
else if ($mode == "login")
{
	login();
}
else if ($mode == "edit")
{
	edit_entry();
}
else if ($mode == "comment")
{
	comment_entry();
}
else if ($mode == "delete")
{
	delete_entry();
}
else if ($mode == "upload")
{
	upload_file();
}
else if ($mode == "help")
{
	displayText("help.txt");	
}
else
{
	displayText("start.txt");

	entryCounter();

	birthdays();
}

if ($db->count())
	echo "<font color=\"#808080\">Queryies: ".$db->count()."</font>\n";

$page->stop();

?>
